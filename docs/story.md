## Inspiration and Backstory

### 2014
This project started as an idea in `2014` to build a website where people could play games. Game websites of the day seemed bothersome with advertisements and a small rectangular area that showed the game on the page.

<hr>

✨ **Inspiration**: Is it possible to create a website where the game being played is `fullscreen` by default?

🔎 **Why?**: Games are captivating experiences. They have the potential to capture our imagination. It seems like additional padding surrounding the game frame by itself distracts from the core experience intended by the game designer.

Note: Fullscreen refers to having the rectangular viewport of the game match the size of the browser window.

<hr>

The creator of the website, named Pius Nyakoojo, called this initial site something random: his favorite color (red) + his 2nd favorite fruit (banana) and hence the name `Rednana`.

### 2015
In `2015`, on new years day, Pius released his first game. Throughout the previous year Pius worked with his brother Bonnie to release `Oz Experiment`.

### 2016
`Rednana` was developed in `2016` with `Oz Experiment` as the first game published. The site that was once only a dream now had a physical presence. Rednana was built using HTML, CSS and JavaScript for the web stack and Firebase for the server stack. It wasn't built with modern tools like grunt, gulp or webpack. It used neither npm nor Node.js. At the time, these tools were unknown to the creator of the site.

### 2017


### 2018


### 2019
This project repository was created.
