## Related Work
A feature comparison with well-known gaming clients

###### [Enoplay](https://enoplay.com)
- 🖥️ **Immersive Gameplay**: Gameplay is immersive by default (no ads, no recommendations, only fullscreen)
- 💬 **Friend Chat**: Find and chat with friends from anywhere on the platform
- 💬 **Community Chat**: Realtime chat with game people in the same game communities integrated via Slack
- ⌨️ **Play Controls**: Easy-to-read instructions on what keyboard or mouse inputs do in a game
- 👫 **Peer-to-peer (p2p)**: User-distributed data storage for low-cost scalability using DatArchive, IPFS and WebTorrent
- 📡 **Works Offline**: Supports offline play using DatArchive and Service Worker
- 🤑 **Players get paid**: Join cryptocurrency programs. ie. Universal Basic Income (UBI) and Player Participation programs (WIP)
- 🤑 **Publishers get paid**: Join cryptocurrency programs. ie. UBI and Publisher Participation programs (WIP)
- 📖 **Open Source**: Code is written to be easily fork-able, easy to re-brand. Republish to your heart's content
- ♻️ **Multi-platform**: Play from virtually any modern web browser available on Windows, Mac and Linux
- 👐 **Open Publication**: Publish any game. Users can still filter content to see what they like. Please be kind.

###### [Steam](https://store.steampowered.com/)
- 🖥️ **Immersive Gameplay**: Gameplay is fullscreen with options to minimize (no additional clutter)
- 💬 **Friend Chat**: Find and chat with friends from anywhere on the platform
- 📡 **Works Offline**: Play your games without a network connection
- 🤑 **Publishers get paid**: Sell games to make money. 30/70 revenue split. 30% goes to Steam, 70% goes to you for each sale
- ♻️ **Multi-platform**: Play from virtually any operating system (Windows, Mac and Linux)
- 👐 **Open Publication**: Anyone can publish any game (if it's appropriate with Steam's [terms of service](https://partner.steamgames.com/steamdirect))

###### [Epic Games Store](https://www.epicgames.com/store/en-US/)
- 🖥️ **Immersive Gameplay**: Gameplay is fullscreen with options to minimize (no additional clutter)
- 📡 **Works Offline**: Play your games without a network connection
- 🤑 **Publishers get paid**: Sell games to make money. 12/88 revenue split. 12% goes to Epic, 88% goes to you for each sale
- ♻️ **Multi-platform**: Play from virtually any operating system (Windows and Mac)
- 👐 **Open Publication**: Anyone can publish any game (if it's appropriate with Epic's [terms of service](https://www.epicgames.com/site/en-US/tos))

###### [Battle.net](https://us.battle.net/account/download/)
- 🖥️ **Immersive Gameplay**: Gameplay is fullscreen with options to minimize (no additional clutter)
- 💬 **Friend Chat**: Find and chat with friends from anywhere on the platform
- 📡 **Works Offline**: Play your games without a network connection
- 🤑 **Publishers get paid**: Blizzard makes 100% of the money
- ♻️ **Multi-platform**: Play from virtually any operating system (Windows, Mac and Linux)
- 👊 **Closed Publication**: Only Blizzard's games. Good for keeping out games that don't meet Blizzard's standards

###### [itch.io](https://itch.io/)
- 📡 **Works Offline**: Use the desktop application to access your games without a network connection
- 🤑 **Publishers get paid**: Sell games to make money. Revenue split with platform is configurable to what you think is fair
- 📖 **Open Source**: Desktop application is [open source](https://github.com/itchio) under MIT license
- ♻️ **Multi-platform**: Play from virtually any operating system (Windows, Mac and Linux) and modern web browser
- 👐 **Open Publication**: Anyone can publish any game (if it's appropriate with itch.io's [terms of service](https://itch.io/docs/legal/terms))

###### [Game Jolt](https://gamejolt.com/)
- 🖥️ **Immersive Gameplay**: Gameplay defaults to fullscreen. No ads, no recommendations, only immersion.
- 🤑 **Publishers get paid**: 30/70 ad-based revenue split
- 📖 **Open Source**: Frontend site is [open source](https://github.com/gamejolt) under MIT license
- ♻️ **Multi-platform**: Play from virtually any modern web browser available on Windows, Mac and Linux
- 👐 **Open Publication**: Anyone can publish any game (if it's appropriate with Game Jolt's [terms of service](https://gamejolt.com/terms))

###### [Miniclip](https://www.miniclip.com/games/en/)
- 🤑 **Publishers get paid**: Ad-based revenue split with platform
- ♻️ **Multi-platform**: Play from virtually any modern web browser available on Windows, Mac and Linux
- 👐 **Open Publication**: Anyone can publish any game (if it's appropriate with Miniclip's [terms of service](https://www.miniclip.com/games/page/en/terms-and-conditions/))

###### [Kongregate](https://www.kongregate.com/)
- 🤑 **Publishers get paid**: 30/70 revenue split when players spend kreds (platform currency) in-game. Up to 50/50 in ad revenue
- ♻️ **Multi-platform**: Play from virtually any modern web browser available on Windows, Mac and Linux
- 👐 **Open Publication**: Anyone can publish any game (if it's appropriate with Kongregate's [terms of service](https://www.kongregate.com/pages/uploader-agreement))

###### [Newgrounds](https://www.newgrounds.com/)
- ♻️ **Multi-platform**: Play from virtually any modern web browser available on Windows, Mac and Linux
- 👐 **Open Publication**: Anyone can publish any game (if it's appropriate with Newground's [terms of service](https://www.newgrounds.com/wiki/help-information/terms-of-use/game-and-movie-guidelines))

###### [Simmer.io](https://simmer.io/)
- 🤑 **Publishers get paid**: Ad-based revenue split with platform
- ♻️ **Multi-platform**: Play from virtually any modern web browser available on Windows, Mac and Linux
- 👐 **Open Publication**: Anyone can publish any game (if it's appropriate with Simmer.io's [terms of service](https://simmer.io/terms))
