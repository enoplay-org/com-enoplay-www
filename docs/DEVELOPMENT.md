# Development Guide for `com-enoplay-www`

## Project Setup
`com-enoplay-www` is built with [Node.js](https://nodejs.org/en/) and uses [npm](https://www.npmjs.com/) to manage external dependencies.

#### Command Line
```
# Check to ensure you are using node.js v10.15.0 or later
node --version

# Check to ensure you are using npm 6.8.0 or later
npm --version
```

###### Install external dependencies
```
npm install
```

###### Compiles and hot-reloads for development
```
npm run serve
```

###### Compiles and minifies for production
```
npm run build
```

###### Run your tests
```
npm run test
```

###### Lints and fixes files
```
npm run lint
```

###### Run your end-to-end tests
```
npm run test:e2e
```

###### Run your unit tests
```
npm run test:unit
```

###### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
