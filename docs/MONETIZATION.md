# Monetization Strategy for `com-enoplay-www`

This strategy is a **work-in-progress** in determining how to pay people for using the Enoplay game service.

**Currency Providers**
The overall strategy is to have a currency provider that creates currency out of thin air and distributes it to people who benefit the Enoplay game network (i.e. content curators and publishers).

**USD Currency Providers**
The currency providers for the US Dollar are banks and the US government and the currency is distributed to people who take out loans, or benefit from government incentives.

**Trust**
Currency providers are incentivized to distribute the currency fairly, i.e. in accordance with the expectations of the users of that currency. When these expectations aren't met, users are free to switch to another currency provider (i.e. The Chinese Yuan, The Russian Ruble or Bitcoin).

## Goals
- Pay game players
- Pay game publishers

### How to pay players
- Pay per hour spent on a game
- Pay for upvoting and downvoting content
- Pay for commenting on content

### How to pay publishers
- Pay per uploaded content
- Pay per engagement by players

## Problems
- ~~Ensuring player is a human (not a bot)~~ - Shouldn't bots be allowed to play? AI agents are important citizens (if not now, in the future)
- ~~Ensuring publishers are uploading original content~~ - Originality is difficult to measure. People are constantly building on one another's work
- Ensuring payments are distributed fairly

### Solutions
- Have multiple solutions for determining authentic player interaction
  - Currency providers need to trust an authenticator to determine which players to pay
  - Otherwise, currency providers can setup their own authenticator system for determining which players to pay
- Have multiple solutions for determining authentic publisher interaction
  - Currency providers need to trust an authenticator. This allows the currency provider to distribute the currency based on publisher's statistics
  - Otherwise, currency providers can manage their own authenticator and decide which players and publishers earn the currency
- Have multiple currency providers that pay based on authenticator metrics
  - Players and publishers need to trust that a currency provider is fairly distributing currency
  - Otherwise, the players and publishers can use another currency provider or setup their own
- In summary: A network of peers that pays humans fairly

### Consequences
- People need to trust one another across many different services. Openness is one of the best strategies for any particular service to gain trust.
- There are multiple service providers in the network: player and publisher authenticators, currency providers, web host providers, static asset distribution providers, etc.
- Currency providers in particular need to determine how to distribute their currency. At the very minimum, players and publishers are expected to receive currency based on various statistics (i.e. publishing a popular game)
- Any given currency provider will also decide if they distribute currency to other service providers for the Enoplay game network. These service providers need the currency to support the players and publishers.

## In Summary
- A network of peers that pays ~~humans~~ participants fairly
- Any given peer can change the currency provider if they don't trust how the currency is being distributed
- Currency providers show evidence that the currency is distributed fairly (i.e. to maximize the creation of valuable content)
    - Some evidence may include: open source code, a public ledger of the entire currency supply and the owners, a clear description of how the currency is distributed and why, etc.
