<div style="text-align: center" align="center">
  <a href="https://www.enoplay.com">
    <img src="src/assets/logo-no-outline.png" alt="Enoplay">
  </a>
</div>
<div style="text-align: center" align="center">
  <a href="https://www.enoplay.com">
    <h1>Enoplay</h1>
  </a>
  <h4 style="text-align: center;">A gaming client for the web. Share and play web games.</h4>
  <p align="center">
    <a href=""><img src="https://gitlab.com/enoplay-org/com-enoplay-www/badges/master/coverage.svg" alt="coverage"></a>
    <a href=""><img src="https://gitlab.com/enoplay-org/com-enoplay-www/badges/master/pipeline.svg" alt="pipeline"></a>
    <a href="https://discordapp.com/channels/531842963064619018"><img src="https://img.shields.io/discord/531842963064619018.svg" alt="Join the chat on Discord"></a>
  </p>
  <br><br>
</div>

`com-enoplay-www` is a web application for the Enoplay game network. It is optimized for *desktop* gameplay (com-enoplay-m is optimized for *mobile*).

This project is a **work-in-progress** and does not completely work as suggested by this document. Parts of the system work as suggested, other parts are still being worked on.

## About
**Enoplay** is a gaming client for the web. The website supports sharing game applications, discovering new games and playing games directly in a web browser.

**Sharing games** is easily done by uploading a zip file containing an index.html and other file assets. **Discovering games** that are fun to play is accomplished using social features like chat and voice communication. **Playing games** in a web browser makes it easy to access games in a safe environment on virtually any device.

## Features
- 🖥️ **Immersive Gameplay**: Gameplay is immersive by default (no ads, no recommendations, only fullscreen)
- 💬 **Friend Chat**: Find and chat with friends from anywhere on the platform
- 💬 **Community Chat**: Realtime chat with game people in the same game communities integrated via Slack
- ⌨️ **Play Controls**: Easy-to-read instructions on what keyboard or mouse inputs do in a game
- 👫 **Peer-to-peer (p2p)**: User-distributed data storage for low-cost scalability using DatArchive, IPFS and WebTorrent
- 📡 **Works Offline**: Supports offline play using DatArchive and Service Worker
- 🤑 **Players get paid**: Join cryptocurrency programs. ie. Universal Basic Income (UBI) and Player Participation programs (WIP)
- 🤑 **Publishers get paid**: Join cryptocurrency programs. ie. UBI and Publisher Participation programs (WIP)
- 📖 **Open Source**: Code is written to be easily fork-able, easy to re-brand. Republish to your heart's content
- ♻️ **Multi-platform**: Play from virtually any modern web browser available on Windows, Mac and Linux
- 👐 **Open Publication**: Publish any game you want
- 🛡️ **Safety**: Users can filter content to see what they want

## Limitations
- 🧪 **Experimental**: This project is a **work-in-progress**

## Additional Notes
- CSS/SASS files aren't organized properly (ie. for use in long-term development)
- There aren't any unit tests for the service usecases
- There aren't any end-to-end tests for the project
- The architecture design isn't complete
- There are a lot of undocumented ideas from the project developer

## Project Development Team
- Pius Nyakoojo - [Gitlab](https://gitlab.com/piusnyakoojo) | [Twitter](https://www.twitter.com/piusnyakoojo)

## Partial Documentation
Things that need to be implemented and aren't written down anywhere else to the best of our knowledge. - project development team

- How to implement search query functionality.
- How to publish a game.
- How to store wallet keys in the browser.
- How to store user keys in the browser.
- How to update data storage settings.
- How to mirror game or user data (similar to mirror websites).
- How to implement ActivityPub for federated interoperability.
- How to make a cryptocurrency with support for a universal basic income.
- How to ensure everyone's data is private by default in a distributed system.
- How to implement 'this game is inspired by these other games' feature for useful search (similar to page rank).
- How to make everything work forever.
- Where to publish the website (gitlab pages? heroku? ipfs?). Beaker Browser's `dat://` is also a possible host but is neither supported by Chrome, Firefox nor Safari.
- Whether to use browser's built-in pseudorandom number generator or write our own.

## Browser Support
Enoplay supports all browsers that are ES5-compliant (IE8 and below are not supported).

## Play on the Website
Go to [enoplay.com](https://www.enoplay.com) and play from the comfort of your browser! Sharpen your skills and compete against other players around the globe. Good luck in your journey, hero!

## Screenshots

Home | Play w/ Chat | Play w/ Play Controls
:--------:|:-----------------:|:--------------------------:
<img src="docs/screenshots/home_page.png" /> | <img src="docs/screenshots/play_page_with_chat.png" />  | <img src="docs/screenshots/play_page_with_play_controls.png" />

Game  | Upload w/ general details | Upload w/ app details
:----:|:-------------------------:|:--------------------:
<img src="docs/screenshots/game_page.png" /> | <img src="docs/screenshots/upload_page_with_general_details.png" />  |  <img src="docs/screenshots/upload_page_with_app_details.png" />

## Documentation
To check out project-related documentation, please visit [docs](docs/README.md)

## Contributing
Everyone can contribute. All are welcome. Please see [contributing guide](https://gitlab.com/enoplay-org/com-enoplay-www/blob/master/CONTRIBUTING.md)

## Acknowledgements
Enoplay depends on a lot of open source software or APIs exposed by software services. We'd like to thank the contributors of these projects for being open, Thank you.

Thanks to the many people who contribute to the open ecosystem.

### Software dependencies
Tooling        |Database     |Webhost      | Chat    | Comments | Videos
-----------------|------------------|------------------|-----------|-----------------|------------
[Vue.js](https://vuejs.org/)         |[WebDB](https://github.com/beakerbrowser/webdb)         |[DatArchive](https://beakerbrowser.com/docs/apis/dat)  |[Slack](https://api.slack.com/web)     | --               | [Youtube](https://developers.google.com/youtube/v3/)
[Vuetify](https://vuetifyjs.com/en/)        |[Localforage](https://github.com/localForage/localForage)  |[WebTorrent](https://github.com/webtorrent/webtorrent) + [Service Worker](https://developer.mozilla.org/en-US/docs/Web/API/Service_Worker_API)                   |            |                   |
[TypeScript](https://www.typescriptlang.org/)  |[OrbitDB](https://github.com/orbitdb/orbit-db)  |[IPFS](https://github.com/ipfs/js-ipfs) + [Service Worker](https://developer.mozilla.org/en-US/docs/Web/API/Service_Worker_API)   |            |                   |

## Learn More
🕹️ Play on the [website](https://www.enoplay.com)<br>
🎬 Watch a video on how to use the website<br>
🎬 Watch a video on how to fork this project<br>
🎬 Watch a video on how this project is made<br>

## Related Work
A [comparison](docs/related-work.md) with well-known gaming clients and the features they support.

Some well-known **gaming clients** include: Desktop ([Steam](https://store.steampowered.com/), [Epic Games Store](https://www.epicgames.com/store/en-US/), [Battle.net](https://us.battle.net/account/download/)), Desktop and Web Browser ([itch.io](https://itch.io/), [Game Jolt](https://gamejolt.com/)), Web Browser ([Miniclip](https://www.miniclip.com/games/en/), [Kongregate](https://www.kongregate.com/), [Newgrounds](https://www.newgrounds.com/), [Simmer.io](https://simmer.io/), [Enoplay](https://www.enoplay.com)), Web Browser + Cloud-streaming ([Google Stadia](https://en.wikipedia.org/wiki/Google_Stadia)), Mobile Store ([Apple App Store](https://www.apple.com/ios/app-store/), [Google Play Store](https://play.google.com/store))

## License
MIT

## Inspiration and Backstory
Read about what inspired the development of this project and its history on the [inspiration and backstory](docs/story.md) page.
