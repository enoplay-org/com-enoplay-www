import Vue from 'vue'
import VueRouter, { RouteConfig, Route } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import GameView from '../views/GameView.vue'
import GamePlayView from '../views/GamePlayView.vue'
import UserView from '../views/UserView.vue'
import SearchView from '../views/SearchView.vue'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  { path: '/', name: 'home', component: HomeView },
  { path: '/game/:gameId', name: 'game', component: GameView },
  { path: '/play/:gameId', name: 'play', component: GamePlayView },
  { path: '/user/:userId', name: 'user', component: UserView },
  { path: '/search', name: 'search', component: SearchView }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
  scrollBehavior (to: Route, _: any, savedPosition: any) {
    if (to.hash) {
      return { selector: to.hash }
    } else if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  }
})

export default router
