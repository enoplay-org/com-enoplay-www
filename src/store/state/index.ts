
import { ActionContext } from 'vuex'

import { UserDatabase } from '../../service/usecase/user-database/data-structures/user-database'

interface RootState {
  isMobileDevice?: boolean

  // Website
  showWebsiteHeader?: boolean
  showWebsiteFooter?: boolean

  // Notifications
  showNotificationList: boolean
  notificationList: Notification[]

  // Settings
  userDatabase: UserDatabase
}

class Notification {
  notificationId: string = ''
  notificationType?: string = ''
  notificationMessage?: string = ''

  notificationIconImageUrl?: string = ''
  notificationMoreInformationUrl?: string = ''
  notificationDate?: Date

  notificationMessageHtmlItemList?: MessageHtmlItem[] = []

  constructor (defaultOption?: Notification) {
    Object.assign(this, defaultOption)
  }
}

class MessageHtmlItem {
  htmlItemId: string = ''
  isHtmlItem: boolean = false
  showRainbowBackground?: boolean = false
  iconImageUrl: string = ''
  text: string = ''

  iconImageColor?: string = ''
}

const HTML_ITEM_ID_CHIP = 'v-chip'
const HTML_ITEM_ID_ICON_WITH_TEXT = 'icon-with-text'

const NOTIFICATION_TYPE_ACCOUNT: string = 'account'
const NOTIFICATION_TYPE_TRANSACTION: string = 'transaction'
const NOTIFICATION_TYPE_GROUP: string = 'group'
const NOTIFICATION_TYPE_GENERAL: string = 'general'

declare type RootActionParam = ActionContext<RootState, RootState>

export {
  RootState,
  RootActionParam,
  Notification,
  NOTIFICATION_TYPE_ACCOUNT,
  NOTIFICATION_TYPE_TRANSACTION,
  NOTIFICATION_TYPE_GROUP,
  NOTIFICATION_TYPE_GENERAL,

  MessageHtmlItem,
  HTML_ITEM_ID_CHIP,
  HTML_ITEM_ID_ICON_WITH_TEXT
}

