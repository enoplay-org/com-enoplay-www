
import { ActionContext } from 'vuex'

import { RootState } from './index'

class UserState {
  currentUserAuthenticationInformation?: any
}

declare type UserActionParam = ActionContext<UserState, RootState>

export {
  UserState,
  UserActionParam
}
