import Vue from 'vue'
import Vuex from 'vuex'

import { mutations } from './mutations'

import { userVuexModule } from './modules/user'

import { FirebaseUserAuthenticationProvider } from '../service/provider/service/firebase/authentication/data-structures/user-authentication'
import { FirebaseFirestoreDatabaseProvider } from '../service/provider/service/firebase/firestore/data-structures/database'

import { UserDatabase } from '../service/usecase/user-database/data-structures/user-database'

import { productionFirebaseConfig } from '../env/firebase'

// Firebase Authentication Provider
const firebaseUserAuthenticationProvider = new FirebaseUserAuthenticationProvider(productionFirebaseConfig, {
  useEmulator: process.env.NODE_ENV === 'development'
})

// Firebase Firestore Database Provider
const firebaseFirestoreDatabaseProvider = new FirebaseFirestoreDatabaseProvider(productionFirebaseConfig, {
  useEmulator: process.env.NODE_ENV === 'development'
})

// User Database
const userDatabase = new UserDatabase(firebaseUserAuthenticationProvider, firebaseFirestoreDatabaseProvider)

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    isMobileDevice: true,

    showWebsiteHeader: true,
    showWebsiteFooter: true,

    showNotificationList: false,
    notificationList: [],

    userDatabase: userDatabase
  },
  getters: {},
  mutations,
  actions: {},
  modules: {
    user: userVuexModule
  }
})
