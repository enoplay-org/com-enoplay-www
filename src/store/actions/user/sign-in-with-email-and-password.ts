
import { UserActionParam } from '../../state/user'

import { SET_CURRENT_USER_AUTHENTICATION_INFORMATION } from '../../mutations/user'

async function signInWithEmailAndPassword ({ rootState, commit }: UserActionParam, {
  emailAddress,
  password
}: {
  emailAddress: string,
  password: string
}) {
  const authenticationInformation = await rootState.userDatabase.userAuthenticationProvider?.signInWithEmailAndPassword(emailAddress, password)
  commit(SET_CURRENT_USER_AUTHENTICATION_INFORMATION, authenticationInformation)
  return authenticationInformation
}

export {
  signInWithEmailAndPassword
}
