
import { UserActionParam } from '../../state/user'

// import { SET_CURRENT_USER_AUTHENTICATION_INFORMATION } from '../../mutations/user'

async function signUpWithEmailAndPassword ({ rootState, commit }: UserActionParam, {
  emailAddress,
  password
}: {
  emailAddress: string,
  password: string
}) {
  const authenticationInformation = await rootState.userDatabase.userAuthenticationProvider?.signUpWithEmailAndPassword(emailAddress, password)
  return authenticationInformation
}

export {
  signUpWithEmailAndPassword
}
