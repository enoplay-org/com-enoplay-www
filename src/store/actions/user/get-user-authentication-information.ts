
import { UserActionParam } from '../../state/user'

import { SET_CURRENT_USER_AUTHENTICATION_INFORMATION } from '../../mutations/user'

async function getUserAuthenticationInformation ({ rootState, commit }: UserActionParam) {
  const currentUserAuthenticationInformation = rootState.userDatabase.userAuthenticationProvider?.getUserAuthenticationInformation({
    authStateEventListenerFunction: (user: any) => {
      if (user) {
        commit(SET_CURRENT_USER_AUTHENTICATION_INFORMATION, user)
      } else {
        commit(SET_CURRENT_USER_AUTHENTICATION_INFORMATION, null)
      }
    }
  })
  commit(SET_CURRENT_USER_AUTHENTICATION_INFORMATION, currentUserAuthenticationInformation)
  return currentUserAuthenticationInformation
}

export {
  getUserAuthenticationInformation
}
