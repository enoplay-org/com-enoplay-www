
import { UserActionParam } from '../../state/user'

async function logout ({ rootState }: UserActionParam) {
  return rootState.userDatabase.userAuthenticationProvider?.logout()
}

export {
  logout
}
