
import { ActionTree } from 'vuex'

import { RootState } from '../../state'
import { UserState } from '../../state/user'

import { getUserAuthenticationInformation } from './get-user-authentication-information'
import { signUpWithEmailAndPassword } from './sign-up-with-email-and-password'
import { signInWithEmailAndPassword } from './sign-in-with-email-and-password'
import { logout } from './logout'

const actions: ActionTree<UserState, RootState> = {
  getUserAuthenticationInformation,
  signUpWithEmailAndPassword,
  signInWithEmailAndPassword,
  logout
}

export {
  actions
}
