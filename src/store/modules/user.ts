
// import Vuex, { Module } from 'vuex'
// import { Vue } from '../../../vue/constants/vue'
import { Module } from 'vuex'

import { RootState } from '../state'
import { UserState } from '../state/user'

import { actions } from '../actions/user'
import { mutations } from '../mutations/user'
import { getters } from '../getters/user'

// Vue.use(Vuex)

const userVuexModule: Module<UserState, RootState> = {
  namespaced: true,
  state: {
    currentUserAuthenticationInformation: null
  },
  getters,
  mutations,
  actions
}

export {
  userVuexModule
}
