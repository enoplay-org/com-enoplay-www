
/* eslint-disable */

const gameTable: { [gameId: string]: any } = {
  'world-of-warcraft': {
    gameId: 'world-of-warcraft',
    name: 'World of Warcraft',
    publisherId: 'blizzard-entertainment',
    tagIdList: ['Action', 'Adventure'],
    price: 19.99,
    isPriceRecurring: false,
    priceRecurrencePeriod: 30 * 24 * 60 * 60 * 1000, // 30 days
    thumbnailIconUrl: `https://i.ytimg.com/an_webp/ZkpMifOi-Yo/mqdefault_6s.webp?du=3000&sqp=CIC7k5IG&rs=AOn4CLCayWWPD-D3I_Un5Voy9M0wpTxhLQ`,
    imagePreviewList: [
      `https://i.ytimg.com/an_webp/ZkpMifOi-Yo/mqdefault_6s.webp?du=3000&sqp=CIC7k5IG&rs=AOn4CLCayWWPD-D3I_Un5Voy9M0wpTxhLQ`,
      `https://i.ytimg.com/an_webp/I02Imlp05MQ/mqdefault_6s.webp?du=3000&sqp=COjCk5IG&rs=AOn4CLA7nFKITOm-kQwZ6-HT47R6AeauXg`,
      `https://i.ytimg.com/an_webp/PshKOmnHaYc/mqdefault_6s.webp?du=3000&sqp=CLawk5IG&rs=AOn4CLD6Z9bh4VnGMVPApwfwOQ6Gu31HsQ`
    ],
    videoPreviewList: [
      `https://www.youtube.com/embed/I02Imlp05MQ`,
      `https://www.youtube.com/embed/0GqgadOS0xA`
    ],
    minimumDescription: '9.2 Eternity’s End. Shadowlands Ending Lore. All Cutscenes. From the climax of Lordaeron to the depths of the Maw, Patch 9.2 heralds the end of reality. Includes Anduin & Sylvanas’ Betrayal, Jailer Zovaal’s Ultimate Plan, Denathrius’ Dread Lords & Arthas-Uther’s Story. Also Tyrande goes angry then calms down. ',
    maximumDescription: `9.2 Eternity’s End. Shadowlands Ending Lore. All Cutscenes. From the climax of Lordaeron to the depths of the Maw, Patch 9.2 heralds the end of reality. Includes Anduin & Sylvanas’ Betrayal, Jailer Zovaal’s Ultimate Plan, Denathrius’ Dread Lords & Arthas-Uther’s Story. Also Tyrande goes angry then calms down. 

    Please Like/Share/Subscribe to show your support! ^_^ Save the Murlocs and have a cookie of your choice!
    
    Chapters:
    0:00 Road to Eternity's End
    Ch1. The Lion & The Banshee: 2:38
    Ch2. Chains of Domination: 7:45
    Ch3. End of Reality:  12:22
    Ch4. The DreadLords: 16:40
    Ch5. Elune & Tyrande: 25:47
    Ch6. Arthas & Uther: 36:01
    Ch7. Eternity's End: 47:49
    
    Of course I added the music in the background silly. I thought this was like a given by now!
    ----------------------------------------------------------------------------------------------------------
    **More Warcraft Lore to add to your playlist & viewing pleasure!**
    Varian Wrynn's Story & Sacrifice [Full Warcraft Lore]
    Link: https://www.youtube.com/watch?v=07XUB...
    
    Illidan Stormrage's Story & Sacrifice [Full Warcraft Lore]
    Link: https://www.youtube.com/watch?v=UeooZ...
    
    All WoW Raid Ending Cinematics [TBC - BFA]
    Link: https://www.youtube.com/watch?v=oXuw1...
    
    Shadowlands Launch Movie: (Up to 9.1 Chains of Domination)
    Link: https://www.youtube.com/watch?v=SGQqA...
    
    BFA Story Movie: (All Cinematics from 8.0 to 8.3)
    Link: https://www.youtube.com/watch?v=wcVYs...
    
    Legion Cinematics: (All Cinematics from 7.0 to 7.3.5)
    Link: https://www.youtube.com/playlist?list...
    
    Warcraft 3 Full Story: (All Reforged Cinematics)
    https://www.youtube.com/playlist?list...
    ----------------------------------------------------------------------------------------------------------
    #wow #cinematics #Shadowlands
    
    Thank you for your continued support~!
    -Ricky Zulpel
    -Farthix921
    -Thanasi Halim
    -Jenny
    -Molly
    
    (If you'd like to support, but don't want your name listed, let me know and will kindly remove it. Thank you again!)
    
    As an ongoing cancer survivor, Warcraft’s stories continue to be a source of comfort & relief. This is the best way I've found to introduce the wonderful World of Warcraft to old and new players alike, with its rich lore, characters and extreme potential. You can expect a retelling of in-game questlines, dialogue, adventures, cinematics & cutscenes to help players keep up with the ever evolving lore of the game.
    
    It's an incredible experience from start to finish! If that sounds like you, then you're in the right place!
    
    World of Warcraft belongs to the mighty Blizzard Entertainment.
    
    Soundtracks/Images Used: (Note- All Credit for music and/or images used belongs and goes to the artists/creators of this amazing music/art. No Copyright Infringement is intended).
    A mix of Battle for Azeroth OST - from World of Warcraft & Warcraft 3 OST.
    World of Warcraft. Chains of Domination. All Cinematics up to 9.1 Recap. Includes Sylvanas vs. Bolvar, Nathanos vs. Tyrande, Anduin vs. Sylvanas, Mourneblade Reborn, Ysera's Rebirth, Denathrius' Betrayal & Covenant Campaigns with Kael'thas and Kel'thuzad & Arthas Memories.
    
    World of Warcraft & Warcraft 3: Reforged belongs to the mighty Blizzard Entertainment.
    
    World of Warcraft® 
    ©2004 Blizzard Entertainment, Inc. All rights reserved. World of Warcraft, Warcraft and Blizzard Entertainment are trademarks or registered trademarks of Blizzard Entertainment, Inc. in the U.S. and/or other countries.`,
    likeToDislikeRatio: 33
  }
}

const gameList = Object.keys(gameTable).map(gameId => gameTable[gameId])

for (let i = 1; i < 10; i++) {
  gameList.push(gameList[0])
}


export {
  gameTable,
  gameList
}

