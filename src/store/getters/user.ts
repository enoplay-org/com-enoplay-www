
import { GetterTree } from 'vuex'

import { RootState } from '../state'
import { UserState } from '../state/user'

const getters: GetterTree<UserState, RootState> = {
  isAuthenticatedUser (state: UserState): boolean {
    // eslint-disable-next-line
    return state.currentUserAuthenticationInformation ? true : false
  }
}

export {
  getters
}
