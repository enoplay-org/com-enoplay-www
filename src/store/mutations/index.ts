
// import Vue from 'vue'
import { MutationTree } from 'vuex'
import { RootState, Notification } from '../state'

const TOGGLE_IS_MOBILE_DEVICE = 'TOGGLE_IS_MOBILE_DEVICE'

const TOGGLE_SHOW_WEBSITE_HEADER = 'TOGGLE_SHOW_WEBSITE_HEADER'
const TOGGLE_SHOW_WEBSITE_FOOTER = 'TOGGLE_SHOW_WEBSITE_FOOTER'

const ADD_NOTIFICATION = 'ADD_NOTIFICATION'
const REMOVE_NOTIFICATION = 'REMOVE_NOTIFICATION'
const TOGGLE_SHOW_NOTIFICATION_LIST = 'TOGGLE_SHOW_NOTIFICATION_LIST'

const mutations: MutationTree<RootState> = {

  [TOGGLE_IS_MOBILE_DEVICE] (state, isMobile: boolean) {
    state.isMobileDevice = isMobile
  },

  // Website

  [TOGGLE_SHOW_WEBSITE_HEADER] (state, isVisible: boolean) {
    state.showWebsiteHeader = isVisible
  },

  [TOGGLE_SHOW_WEBSITE_FOOTER] (state, isVisible: boolean) {
    state.showWebsiteFooter = isVisible
  },

  [ADD_NOTIFICATION] (state, notification: Notification) {
    // Vue.set(state.notificationList, state.notificationList.length, notification)

    state.notificationList.unshift(notification)

    // Remove the notification after 10 seconds
    setTimeout(() => {
      const indexOfNotification = state.notificationList.map((notificationItem: Notification) => notificationItem.notificationId).indexOf(notification.notificationId)

      if (indexOfNotification >= 0) {
        state.notificationList.splice(indexOfNotification, 1)
      }

      if (state.notificationList.length === 0) {
        state.showNotificationList = false
      }
    }, 10 * 1000)

    // No more than 10 notifications at a time
    if (state.notificationList.length > 10) {
      state.notificationList.pop()
    }

    state.showNotificationList = true
  },

  [REMOVE_NOTIFICATION] (state, notificationIndex: number) {
    state.notificationList.splice(notificationIndex, 1)

    if (state.notificationList.length === 0) {
      state.showNotificationList = false
    }
  },

  [TOGGLE_SHOW_NOTIFICATION_LIST] (state, isVisible?: boolean) {
    state.showNotificationList = isVisible !== undefined ? isVisible : !state.showNotificationList
  }
}

export {
  mutations,
  TOGGLE_IS_MOBILE_DEVICE,
  TOGGLE_SHOW_WEBSITE_HEADER,
  TOGGLE_SHOW_WEBSITE_FOOTER,
  ADD_NOTIFICATION,
  REMOVE_NOTIFICATION,
  TOGGLE_SHOW_NOTIFICATION_LIST
}
