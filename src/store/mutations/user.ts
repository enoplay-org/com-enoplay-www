
// import Vue from 'vue'
import { MutationTree } from 'vuex'
// import { RootState } from '../state'
import { UserState } from '../state/user'

const SET_CURRENT_USER_AUTHENTICATION_INFORMATION = 'SET_CURRENT_USER_AUTHENTICATION_INFORMATION'

const mutations: MutationTree<UserState> = {
  [SET_CURRENT_USER_AUTHENTICATION_INFORMATION] (state, userAuthenticationInformation: string) {
    state.currentUserAuthenticationInformation = userAuthenticationInformation
  }
}

export {
  mutations,
  SET_CURRENT_USER_AUTHENTICATION_INFORMATION
}
