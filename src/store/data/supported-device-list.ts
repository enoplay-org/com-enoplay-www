
const SUPPORTED_DEVICE_LIST = [
  { itemName: 'Mobile Phone', itemIconId: 'phone_iphone' },
  { itemName: 'Mobile Tablet', itemIconId: 'tablet_mac' },
  { itemName: 'Desktop', itemIconId: 'desktop_windows' },
  { itemName: 'Gamepad', itemIconId: 'sports_esports' },
  { itemName: 'Virtual Reality Headset', itemIconId: 'visibility' },
  { itemName: 'Keyboard', itemIconId: 'keyboard' },
  { itemName: 'Mouse', itemIconId: 'mouse' },
  { itemName: 'Brain Computer Interface', itemIconId: 'psychology' }
]

export {
  SUPPORTED_DEVICE_LIST
}
