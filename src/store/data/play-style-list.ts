
const PLAY_STYLE_LIST = [
  { itemName: 'Singleplayer', itemIconId: 'person' },
  { itemName: 'Multiplayer (2+)', itemIconId: 'people' },
  { itemName: 'Massive Multiplayer (100+)', itemIconId: 'groups' },
  { itemName: 'Local Co-Op', itemIconId: 'group_add' }
]

export {
  PLAY_STYLE_LIST
}
