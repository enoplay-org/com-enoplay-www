
const DEVELOPMENT_CATEGORY_LIST = [
  { itemName: 'Fully Complete Game', itemIconId: 'verified' },
  { itemName: 'Hackathon Project', itemIconId: 'help' },
  { itemName: 'Demo', itemIconId: 'help' },
  { itemName: 'Prerelease', itemIconId: 'help' },
  { itemName: 'In-Development', itemIconId: 'settings' }
]

export {
  DEVELOPMENT_CATEGORY_LIST
}
