
const TAG_LIST = [
  { itemName: 'Action', itemIconId: 'local_fire_department' },
  { itemName: 'Adventure', itemIconId: 'forest' },
  { itemName: 'In Real Life (IRL)', itemIconId: 'directions_run' },
  { itemName: 'Sports', itemIconId: '' }
]

export {
  TAG_LIST
}
