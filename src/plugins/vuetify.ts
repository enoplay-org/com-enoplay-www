import Vue from 'vue'
import Vuetify from 'vuetify/lib/framework'

import 'material-design-icons-iconfont/dist/material-design-icons.css'
import 'vuetify/dist/vuetify.min.css'

Vue.use(Vuetify)

const vuetify = new Vuetify({
  theme: { dark: true },
  icons: {
    iconfont: 'md'
  }
})

export {
  vuetify
}
