
class User {
  id: string = ''
  alias: string = ''
  username: string = ''
}

export {
  User
}
