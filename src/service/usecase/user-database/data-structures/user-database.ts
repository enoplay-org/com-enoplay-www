
import { UserAuthenticationProvider } from '../../../provider/service-type/user-authentication'
import { DatabaseProvider } from '../../../provider/service-type/database'

class UserDatabase {
  userAuthenticationProvider?: UserAuthenticationProvider
  databaseProvider?: DatabaseProvider

  constructor (userAuthenticationProvider?: UserAuthenticationProvider, databaseProvider?: DatabaseProvider) {
    this.userAuthenticationProvider = userAuthenticationProvider
    this.databaseProvider = databaseProvider
  }
}

export {
  UserDatabase
}
