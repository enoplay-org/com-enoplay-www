
import { UserDatabase } from '../../../data-structures/user-database'
import { User } from '../../../data-structures/user'

import { userStoreId } from '../../../constants/user-database'

async function createAccount (this: UserDatabase, options?: {
  signInWithEmailAndPassword?: boolean
  signInWithGoogle?: boolean
  emailAddress?: string
  password?: string
}): Promise<User> {
  let authenticatedUserInformation

  if (options?.signInWithEmailAndPassword) {
    authenticatedUserInformation = await this.userAuthenticationProvider?.signUpWithEmailAndPassword(options.emailAddress!, options.password!)
    authenticatedUserInformation = await this.userAuthenticationProvider?.signInWithEmailAndPassword(options.emailAddress!, options.password!)
  } else if (options?.signInWithGoogle) {
    authenticatedUserInformation = await this.userAuthenticationProvider?.signInWithGoogle()
  }

  const newUser = new User()
  newUser.id = authenticatedUserInformation.uid
  newUser.alias = newUser.id

  await this.databaseProvider?.createItem(userStoreId, newUser.id, newUser)

  return newUser
}

export {
  createAccount
}
