
interface UserAuthenticationProvider {

  getUserAuthenticationInformation (options?: any): any

  signInWithEmailAndPassword (emailAddress: string, password: string): any
  signInWithGoogle (): any

  signUpWithEmailAndPassword (emailAddress: string, password: string): any

  logout (): any

  sendPasswordRecoveryCode (): any
}

export {
  UserAuthenticationProvider
}
