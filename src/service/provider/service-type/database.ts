
interface DatabaseProvider {
  getItem (storeId: string, itemId: string, options?: any): Promise<any>
  getItemList (storeId: string, options?: any): Promise<any>
  createItem (storeId: string, itemId: string, item: any, options?: any): Promise<any>
  updateItem (storeId: string, itemId: string, item: any, options?: any): Promise<any>
  deleteItem (storeId: string, itemId: string, options?: any): Promise<any>
  addItemEventListener (storeId: string, itemId: string, eventId: string, eventListenerFunction: any, options?: any): Promise<string>
  removeItemEventListener (storeId: string, itemId: string, eventId: string, eventListenerId: string, options?: any): Promise<any>
}

export {
  DatabaseProvider
}
