
import { FirebaseUserAuthenticationProvider } from '../../data-structures/user-authentication'

function signInWithGoogle (this: FirebaseUserAuthenticationProvider) {
  return this.firebaseAuth?.currentUser
}

export {
  signInWithGoogle
}
