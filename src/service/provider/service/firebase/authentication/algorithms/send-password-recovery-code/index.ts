
import { FirebaseUserAuthenticationProvider } from '../../data-structures/user-authentication'

function sendPasswordRecoveryCode (this: FirebaseUserAuthenticationProvider) {
  return this.firebaseAuth?.currentUser
}

export {
  sendPasswordRecoveryCode
}
