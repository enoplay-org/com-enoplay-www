
import firebase from 'firebase/compat/app'
import 'firebase/compat/auth'
import 'firebase/compat/analytics'

import { FirebaseUserAuthenticationProvider } from '../../data-structures/user-authentication'

function initializeService (this: FirebaseUserAuthenticationProvider, firebaseConfig: {
  apiKey: string,
  authDomain: string,
  projectId: string,
  storageBucket: string,
  messagingSenderId: string,
  appId: string,
  measurementId: string
}, options?: {
  useEmulator: boolean
}) {
  this.firebaseApp = firebase.apps.length > 0 ? firebase.app() : firebase.initializeApp(firebaseConfig)
  this.firebaseAnalytics = firebase.analytics(this.firebaseApp!)
  this.firebaseAuth = this.firebaseApp!.auth()

  if (options && options.useEmulator) {
    this.firebaseAuth.useEmulator('http://localhost:9099')
  }
}

export {
  initializeService
}
