
import { FirebaseUserAuthenticationProvider } from '../../data-structures/user-authentication'

function logout (this: FirebaseUserAuthenticationProvider) {
  return this.firebaseAuth?.signOut()
}

export {
  logout
}
