
import { FirebaseUserAuthenticationProvider } from '../../data-structures/user-authentication'

function getUserAuthenticationInformation (this: FirebaseUserAuthenticationProvider, options?: {
  authStateEventListenerFunction?: any
}) {
  this.firebaseAuth?.onAuthStateChanged(options?.authStateEventListenerFunction)
  return this.firebaseAuth?.currentUser
}

export {
  getUserAuthenticationInformation
}
