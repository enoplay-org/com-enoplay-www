
import { FirebaseUserAuthenticationProvider } from '../../data-structures/user-authentication'

function signUpWithEmailAndPassword (this: FirebaseUserAuthenticationProvider, emailAddress: string, password: string) {
  return this.firebaseAuth?.createUserWithEmailAndPassword(emailAddress, password)
}

export {
  signUpWithEmailAndPassword
}
