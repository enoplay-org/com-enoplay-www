
import { FirebaseUserAuthenticationProvider } from '../../data-structures/user-authentication'

async function signInWithEmailAndPassword (this: FirebaseUserAuthenticationProvider, emailAddress: string, password: string) {
  await this.firebaseAuth?.signInWithEmailAndPassword(emailAddress, password)

  console.log('sign in: ', this.firebaseAuth?.currentUser)
  return this.firebaseAuth?.currentUser
}

export {
  signInWithEmailAndPassword
}
