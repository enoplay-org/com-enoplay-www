
import firebase from 'firebase/compat/app'

import { initializeService } from '../algorithms/initialize-service'
import { getUserAuthenticationInformation } from '../algorithms/get-user-authentication-information'
import { logout } from '../algorithms/logout'
import { sendPasswordRecoveryCode } from '../algorithms/send-password-recovery-code'
import { signInWithEmailAndPassword } from '../algorithms/sign-in-with-email-and-password'
import { signInWithGoogle } from '../algorithms/sign-in-with-google'
import { signUpWithEmailAndPassword } from '../algorithms/sign-up-with-email-and-password'

class FirebaseUserAuthenticationProvider {
  firebaseApp?: firebase.app.App
  firebaseAnalytics?: firebase.analytics.Analytics
  firebaseAuth?: firebase.auth.Auth

  constructor (...options: Parameters<typeof initializeService>) {
    this.initializeService(...options)
  }

  initializeService: typeof initializeService = initializeService.bind(this)

  getUserAuthenticationInformation: typeof getUserAuthenticationInformation = getUserAuthenticationInformation.bind(this)
  logout: typeof logout = logout.bind(this)
  sendPasswordRecoveryCode: typeof sendPasswordRecoveryCode = sendPasswordRecoveryCode.bind(this)
  signInWithEmailAndPassword: typeof signInWithEmailAndPassword = signInWithEmailAndPassword.bind(this)
  signInWithGoogle: typeof signInWithGoogle = signInWithGoogle.bind(this)
  signUpWithEmailAndPassword: typeof signUpWithEmailAndPassword = signUpWithEmailAndPassword.bind(this)
}

export {
  FirebaseUserAuthenticationProvider
}
