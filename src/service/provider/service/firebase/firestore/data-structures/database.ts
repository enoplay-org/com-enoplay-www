
import firebase from 'firebase/compat/app'

import { initializeService } from '../algorithms/initialize-service'
import { createItem } from '../algorithms/create-item'
import { getItem } from '../algorithms/get-item'
import { getItemList } from '../algorithms/get-item-list'
import { updateItem } from '../algorithms/update-item'
import { deleteItem } from '../algorithms/delete-item'
import { addItemEventListener } from '../algorithms/add-item-event-listener'
import { removeItemEventListener } from '../algorithms/remove-item-event-listener'

class FirebaseFirestoreDatabaseProvider {
  firebaseApp?: firebase.app.App
  firestore?: firebase.firestore.Firestore

  eventListenerIdMap: { [eventListenerId: string]: any} = {}

  constructor (...options: Parameters<typeof initializeService>) {
    this.initializeService(...options)
  }

  initializeService: typeof initializeService = initializeService.bind(this)
  createItem: typeof createItem = createItem.bind(this)
  getItem: typeof getItem = getItem.bind(this)
  getItemList: typeof getItemList = getItemList.bind(this)
  updateItem: typeof updateItem = updateItem.bind(this)
  deleteItem: typeof deleteItem = deleteItem.bind(this)
  addItemEventListener: typeof addItemEventListener = addItemEventListener.bind(this)
  removeItemEventListener: typeof removeItemEventListener = removeItemEventListener.bind(this)
}

export {
  FirebaseFirestoreDatabaseProvider
}
