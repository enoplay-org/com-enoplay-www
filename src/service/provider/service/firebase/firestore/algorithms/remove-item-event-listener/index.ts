
import { FirebaseFirestoreDatabaseProvider } from '../../data-structures/database'

async function removeItemEventListener (this: FirebaseFirestoreDatabaseProvider, storeId: string, itemId: string, eventId: string, eventListenerId: string, options?: any): Promise<any> {
  if (this.eventListenerIdMap[eventListenerId]) {
    this.eventListenerIdMap[eventListenerId]()
  }
  return eventListenerId
}

export {
  removeItemEventListener
}
