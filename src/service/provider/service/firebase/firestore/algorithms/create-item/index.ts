
import { FirebaseFirestoreDatabaseProvider } from '../../data-structures/database'

async function createItem (this: FirebaseFirestoreDatabaseProvider, storeId: string, itemId: string, item: any, options?: any): Promise<any> {
  await this.firestore?.collection(storeId).doc(itemId).set(item)
  return item
}

export {
  createItem
}
