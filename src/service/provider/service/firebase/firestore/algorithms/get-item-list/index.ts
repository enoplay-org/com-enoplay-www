
import { FirebaseFirestoreDatabaseProvider } from '../../data-structures/database'

async function getItemList (this: FirebaseFirestoreDatabaseProvider, storeId: string, options?: any): Promise<any> {
  return this.firestore?.collection(storeId).limit(10)
}

export {
  getItemList
}
