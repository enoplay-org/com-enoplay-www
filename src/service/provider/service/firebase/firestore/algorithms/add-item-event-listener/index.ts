
import * as uuid from 'uuid'

import { FirebaseFirestoreDatabaseProvider } from '../../data-structures/database'

async function addItemEventListener (this: FirebaseFirestoreDatabaseProvider, storeId: string, itemId: string, eventId: string, eventListenerFunction: any, options?: any): Promise<string> {
  const unsubscribe = this.firestore?.collection(storeId).doc(itemId).onSnapshot((doc) => {
    eventListenerFunction(doc.data())
  })

  const eventListenerId = uuid.v4()
  this.eventListenerIdMap[eventListenerId] = unsubscribe

  return eventListenerId
}

export {
  addItemEventListener
}
