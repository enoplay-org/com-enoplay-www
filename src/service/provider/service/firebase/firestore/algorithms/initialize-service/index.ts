
import firebase from 'firebase/compat/app'
import 'firebase/compat/auth'
import 'firebase/compat/analytics'
import 'firebase/compat/firestore'

import { FirebaseFirestoreDatabaseProvider } from '../../data-structures/database'

function initializeService (this: FirebaseFirestoreDatabaseProvider, firebaseConfig: {
  apiKey: string,
  authDomain: string,
  projectId: string,
  storageBucket: string,
  messagingSenderId: string,
  appId: string,
  measurementId: string
}, options?: {
  useEmulator: boolean
}) {
  this.firebaseApp = firebase.apps.length > 0 ? firebase.app() : firebase.initializeApp(firebaseConfig)
  this.firestore = this.firebaseApp!.firestore()

  if (options && options.useEmulator) {
    this.firestore.useEmulator('http://localhost', 8090)
  }
}

export {
  initializeService
}
