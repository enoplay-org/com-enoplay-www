
import { FirebaseFirestoreDatabaseProvider } from '../../data-structures/database'

async function getItem (this: FirebaseFirestoreDatabaseProvider, storeId: string, itemId: string, options?: any): Promise<any> {
  return this.firestore?.collection(storeId).doc(itemId).get()
}

export {
  getItem
}
