
import { FirebaseFirestoreDatabaseProvider } from '../../data-structures/database'

async function updateItem (this: FirebaseFirestoreDatabaseProvider, storeId: string, itemId: string, item: any, options?: any): Promise<any> {
  await this.firestore?.collection(storeId).doc(itemId).update(item)
  return item
}

export {
  updateItem
}
