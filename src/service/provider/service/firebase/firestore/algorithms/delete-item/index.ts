
import { FirebaseFirestoreDatabaseProvider } from '../../data-structures/database'

async function deleteItem (this: FirebaseFirestoreDatabaseProvider, storeId: string, itemId: string, options?: any): Promise<any> {
  await this.firestore?.collection(storeId).doc(itemId).delete()
  return true
}

export {
  deleteItem
}
